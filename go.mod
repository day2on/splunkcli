module splunkcli

go 1.16

require (
	github.com/spf13/cobra v1.2.1 // indirect
	github.com/spf13/viper v1.8.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
