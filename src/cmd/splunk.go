/*
Copyright © 2021, 73 Prime LLC; Allen Plummer; allen@73prime.io
License extended to The Day2On Project, Copyright © 2021

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bytes"
	"crypto/tls"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)


/*  this interface/implementation is to facilitate mocking/testing */
type SplunkWebInterface interface {
	HttpProcessor(verb, url, username, password string, payload []byte) ([]byte, error)
}
var SplunkClient SplunkWebInterface

type splunkWebImpl struct{}
func (*splunkWebImpl) HttpProcessor(verb, url, username, password string, payload []byte) ([]byte, error) {
	fmt.Printf("Using URL: %s, with verb: %s\n", url, verb)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	req, _ := http.NewRequest(verb, url, bytes.NewBuffer(payload))
	req.SetBasicAuth(username, password)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if resp.StatusCode >=400 {
		err = errors.New(string(body))
	}
	return body, err
}
