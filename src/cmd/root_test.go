/*
Copyright © 2021, 73 Prime LLC; Allen Plummer; allen@73prime.io
License extended to The Day2On Project, Copyright © 2021

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"github.com/spf13/viper"
	"io"
	"io/ioutil"
	"os"
	"testing"
)

func Test_InitConfig(t *testing.T) {
	initConfig()
}
func Test_Ask4Confirm(t *testing.T) {
	//setup the temp file like a beast.
	in, err := ioutil.TempFile("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer in.Close()
	//write the user fake "y"
	_, err = io.WriteString(in, "y\n")
	if err != nil {
		t.Fatal(err)
	}
	//go to the beginning of the temp file
	_, err = in.Seek(0, os.SEEK_SET)
	if err != nil {
		t.Fatal(err)
	}
	answerYes := Ask4confirm("test", in)
	if !answerYes {
		t.Errorf("Ask4Confirm shouldn't have returned %t", answerYes)
	}

	//write the user fake "n"
	_, err = io.WriteString(in, "n\n")
	if err != nil {
		t.Fatal(err)
	}
	//go to the beginning of the temp file
	_, err = in.Seek(0, os.SEEK_SET)
	if err != nil {
		t.Fatal(err)
	}
	answerNo := Ask4confirm("test", in)
	if !answerNo {
		t.Errorf("Ask4Confirm shouldn't have returned %t", answerNo)
	}

	viper.Set("auto-approve", true)
	answerNo = Ask4confirm("test", in)
	if !answerNo {
		t.Errorf("Ask4Confirm shouldn't have returned %t", answerNo)
	}
	//Ask4confirm("test2")
}
func Test_CollectFlags(t *testing.T) {
	expectedSplunkURL := "things"
	expectedSplunkUser := "to look at"
	expectedSplunkPassword := "nicely"
	viper.Set("SPLUNK_URL", expectedSplunkURL)
	viper.Set("SPLUNK_USER", expectedSplunkUser)
	viper.Set("SPLUNK_PASSWORD", expectedSplunkPassword)
	collectFlags()
	if splunkURL != expectedSplunkURL ||
		splunkUser != expectedSplunkUser ||
		splunkPassword != expectedSplunkPassword{
		t.Errorf("Collecting Flags threw an error, things aren't as they seem.")
	}
}
