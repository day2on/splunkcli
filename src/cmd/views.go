/*
Copyright © 2021, 73 Prime LLC; Allen Plummer; allen@73prime.io
License extended to The Day2On Project, Copyright © 2021

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"encoding/xml"
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"io/ioutil"
	"log"
	"net/url"
	"strings"
)

type Dashboard struct {
	XMLName     xml.Name `xml:"dashboard"`
	Label       string   `xml:"label"`
}
//Generated from https://www.onlinetool.io/xmltogo/
type Views struct {
	XMLName    xml.Name `xml:"feed"`
	Text       string   `xml:",chardata"`
	Xmlns      string   `xml:"xmlns,attr"`
	S          string   `xml:"s,attr"`
	Opensearch string   `xml:"opensearch,attr"`
	Title      string   `xml:"title"`
	ID         string   `xml:"id"`
	Updated    string   `xml:"updated"`
	Generator  struct {
		Text    string `xml:",chardata"`
		Build   string `xml:"build,attr"`
		Version string `xml:"version,attr"`
	} `xml:"generator"`
	Author struct {
		Text string `xml:",chardata"`
		Name string `xml:"name"`
	} `xml:"author"`
	Link []struct {
		Text string `xml:",chardata"`
		Href string `xml:"href,attr"`
		Rel  string `xml:"rel,attr"`
	} `xml:"link"`
	TotalResults string `xml:"totalResults"`
	ItemsPerPage string `xml:"itemsPerPage"`
	StartIndex   string `xml:"startIndex"`
	Messages     string `xml:"messages"`
	Entry        []struct {
		Text    string `xml:",chardata"`
		Title   string `xml:"title"`
		ID      string `xml:"id"`
		Updated string `xml:"updated"`
		Link    []struct {
			Text string `xml:",chardata"`
			Href string `xml:"href,attr"`
			Rel  string `xml:"rel,attr"`
		} `xml:"link"`
		Author struct {
			Text string `xml:",chardata"`
			Name string `xml:"name"`
		} `xml:"author"`
		Content struct {
			Text string `xml:",chardata"`
			Type string `xml:"type,attr"`
			Dict struct {
				Text string `xml:",chardata"`
				Key  []struct {
					Text string `xml:",chardata"`
					Name string `xml:"name,attr"`
					Dict struct {
						Text string `xml:",chardata"`
						Key  []struct {
							Text string `xml:",chardata"`
							Name string `xml:"name,attr"`
							Dict struct {
								Text string `xml:",chardata"`
								Key  []struct {
									Text string `xml:",chardata"`
									Name string `xml:"name,attr"`
									List struct {
										Text string   `xml:",chardata"`
										Item []string `xml:"item"`
									} `xml:"list"`
								} `xml:"key"`
							} `xml:"dict"`
						} `xml:"key"`
					} `xml:"dict"`
				} `xml:"key"`
			} `xml:"dict"`
		} `xml:"content"`
	} `xml:"entry"`
}



var viewsCmd = &cobra.Command{
	Use:   "views",
	Short: "Splunk views functionality",
	Long: `This interfaces with the Splunk REST API`,
	//Run: func(cmd *cobra.Command, args []string) {
	//},
}
var GetSplunkViewsCmd = &cobra.Command{
	Use:   "getviews",
	Short: "Gets list of Splunk Views for given app",
	Long: `This interfaces with the Splunk REST API, and gets list of views.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1{
			panic("You must have an app name to use here.")
		}
		GetSplunkViewsForApp(args[0], splunkUser, false)
	},
}
var ExportSplunkViewsCmd = &cobra.Command{
	Use:   "exportviews",
	Short: "Exports list of Splunk Views for given app",
	Long: `This interfaces with the Splunk REST API, and exports list of views.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1{
			panic("You must have an app name to use here.")
		}
		GetSplunkViewsForApp(args[0], splunkUser, true)
	},
}
func GetSplunkViewsForApp(app, user string, export bool) ([]string, error) {
	if splunkDirectory == "" {
		panic("SPLUNK_DIRECTORY needs to be set.")
	}
	//follows the pattern: curl -ku user:password https://localhost:8089/servicesNS/-/<appname>/data/ui/views
	url := fmt.Sprintf("%s/servicesNS/-/%s/data/ui/views", splunkURL, app)
	data, err := SplunkClient.HttpProcessor("GET", url, splunkUser, splunkPassword, nil)
	if err != nil {
		log.Fatal(err)
	}
	views, err := ParseSplunkViews(data, user, export)
	var viewNames []string
	for _, v := range views.Entry {
		if v.Author.Name == user {
			viewNames = append(viewNames, v.Title)
		}
	}
	return viewNames, err
}
func ParseSplunkViews(viewsXML []byte, user string, export bool) (Views, error) {
	var views Views
	err := xml.Unmarshal(viewsXML, &views)
	if err == nil {
		for _, value := range views.Entry {
			if value.Author.Name == user {
				if !export {
					//fmt.Printf("Splunk View #%d: %s\n", idx+1, value.Title)
				}
				for _, value2 := range value.Content.Dict.Key {
					if !export {
						//fmt.Printf("     key: %s, value: %s\n", value2.Name, value2.Text)
					}
					if export && value2.Name == "eai:data" {
						if writeToDirectory(value2.Text, "views",  fmt.Sprintf("%s.xml", value.Title)) {
							fmt.Printf("Exported view %s, writing to file %s/%s.xml", value.Title, splunkDirectory, value.Title)
						}
					}
				}
			}
		}
	} else {
		fmt.Printf("Error: %s", err.Error())
	}
	return views, err
}
var SyncSplunkViewsCmd = &cobra.Command{
	Use:   "syncviews",
	Short: "Syncs directory of Splunk Views for given app",
	Long: `This interfaces with the Splunk REST API, and syncs list of views.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1{
			panic("You must have an app name to use here.")
		}
		PostSplunkViews(args[0])
	},
}
var GetViewCmd = &cobra.Command{
	Use:   "getview",
	Short: "Gets a given view by app name and user.",
	Long: `This interfaces with the Splunk REST API, and gets a view by name.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 2{
			panic("You must have an app name to use here, along with a view name.")
		}
		success, viewText := GetSplunkView(args[0], args[1])
		if success {
			fmt.Println(viewText)
		}
	},
}
var DeleteViewCmd = &cobra.Command{
	Use:   "deleteview",
	Short: "Deletes a given view by app name and user.",
	Long: `This interfaces with the Splunk REST API, and deletes a view by name.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 2{
			panic("You must have an app name to use here, along with a view name.")
		}
		DeleteSplunkView(args[0], args[1])
	},
}
var CreateUpdateViewCmd = &cobra.Command{
	Use:   "createupdateview",
	Short: "Creates or Updates a given view by app name, file, and user.",
	Long: `This interfaces with the Splunk REST API, and creates/updates a view by name.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 2 {
			panic("You must have an app name to use here, along with a file name.")
		}
		CreateUpdateView(args[0], args[1])
	},
}
func GetSplunkView(app, name string) (bool, string) {
	url := fmt.Sprintf("%s/servicesNS/%s/%s/data/ui/views/%s", splunkURL, splunkUser, app, name)
	data, err := SplunkClient.HttpProcessor("GET", url, splunkUser, splunkPassword, nil)
	if err != nil {
		//if error, let's assume it's because could not find the view.
		if strings.Contains(err.Error(), "Could not find") {
			return false, ""
		} else {
			//can't do anything else.
			log.Fatal(err)
		}
	}

	views, err := ParseSplunkViews(data, splunkUser, false)
	if err == nil && len(views.Entry) == 1 {
		for _, value := range views.Entry[0].Content.Dict.Key {
			if value.Name == "eai:data" {
				return true, value.Text
			}
		}
	}
	return false, ""
}
func DeleteSplunkView(app, name string) bool {
	url := fmt.Sprintf("%s/servicesNS/%s/%s/data/ui/views/%s", splunkURL, splunkUser, app, name)
	_, err := SplunkClient.HttpProcessor("DELETE", url, splunkUser, splunkPassword, nil)
	if err != nil {
		//log.Printf("ERROR: " + err.Error())
		if strings.Contains(err.Error(), "Could not find") {
			log.Printf("Could not find view %s in app %s to delete.", name, app)
		} else {
			//can't do anything else.
			log.Fatal(err)
		}
		return false
	}
	log.Printf("Deleted view %s from app %s.", name, app)
	return true
}
func CreateUpdateView(app, fileName string) bool {
	content, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatal(err)
	}
	viewData := string(content)
	//_, file := path.Split(fileName)
	//viewName := strings.TrimSuffix(file, ".xml")
	var dboard Dashboard
	err = xml.Unmarshal(content, &dboard)
	if err != nil {
		log.Fatal(err)
	}
	viewName := dboard.Label
	found, origViewData := GetSplunkView(app, viewName)
	if found {
		//Let's update it, if different.
		if origViewData == viewData {
			fmt.Printf("%s content is the same, skipping...", viewName)
		} else {
			//Update the view
			params := url.Values{}
			params.Add("eai:data", viewData)
			url := fmt.Sprintf("%s/servicesNS/%s/%s/data/ui/views/%s", splunkURL, splunkUser, app, viewName)
			_, err := SplunkClient.HttpProcessor("POST", url, splunkUser, splunkPassword, []byte(params.Encode()))
			if err != nil {
				log.Fatal(err)
			} else {
				log.Printf("Successfully updated view %s in app %s.", viewName, app)
				return true
			}
		}

	} else {
		//Let's create it.
		params := url.Values{}
		params.Add("name", viewName)
		params.Add("eai:data", viewData)
		url := fmt.Sprintf("%s/servicesNS/%s/%s/data/ui/views", splunkURL, splunkUser, app)
		_, err := SplunkClient.HttpProcessor("POST", url, splunkUser, splunkPassword, []byte(params.Encode()))
		if err != nil {
			log.Fatal(err)
		} else {
			log.Printf("Successfully created view %s in app %s.", viewName, app)
			return true
		}
	}
	return false
}
func PostSplunkViews(app string) error {
	if splunkDirectory == "" {
		return errors.New("You need to have SPLUNK_DIRECTORY set.")
	}
	workingDir := fmt.Sprintf("%s/%s", splunkDirectory, "views")
	files, err := ioutil.ReadDir(workingDir)
	if err != nil {
		log.Fatalf("Expecting a 'views' subdirectory in the directory %s.", splunkDirectory)
	}
	var dboard Dashboard
	var localDBoards []Dashboard
	var localDFiles []string
	for _, file := range files {
		if !file.IsDir(){
			//fmt.Printf("Working with file %s\n", file.Name())
			fullFileName := fmt.Sprintf("%s/%s", workingDir, file.Name())
			content, err := ioutil.ReadFile(fullFileName)
			if err != nil {
				log.Fatal(err)
			}
			err = xml.Unmarshal(content, &dboard)
			if err != nil {
				log.Fatal(err)
			}
			localDBoards = append(localDBoards, dboard)
			localDFiles = append(localDFiles, fullFileName)
			//fmt.Printf(dboard.Label)
			//CreateUpdateView(app,file.Name())
		}
	}
	splunkViewNames, err := GetSplunkViewsForApp(app, splunkUser, false)
	if err != nil {
		log.Fatal(err)
	}
	//delete views not captured locally.
	for _, sView := range splunkViewNames {
		if !IsSplunkViewLocal(sView, localDBoards) {
			DeleteSplunkView(app, sView)
		}
	}
	//now, walk through local dashes, and create/update
	for _, localFile := range localDFiles {
		created := CreateUpdateView(app, localFile)
		fmt.Printf("Status of creating %s: %t\n", localFile, created)
	}
	return nil
}
func IsSplunkViewLocal(viewName string, localViews []Dashboard) bool {
	for _, v := range localViews {
		if v.Label == viewName {
			return true
		}
	}
	return false
}
func init() {
	viewsCmd.AddCommand(GetSplunkViewsCmd)
	viewsCmd.AddCommand(ExportSplunkViewsCmd)
	viewsCmd.AddCommand(SyncSplunkViewsCmd)
	viewsCmd.AddCommand(GetViewCmd)
	viewsCmd.AddCommand(DeleteViewCmd)
	viewsCmd.AddCommand(CreateUpdateViewCmd)
	rootCmd.AddCommand(viewsCmd)
	SplunkClient = &splunkWebImpl{}
}
