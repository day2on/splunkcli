/*
Copyright © 2021, 73 Prime LLC; Allen Plummer; allen@73prime.io
License extended to The Day2On Project, Copyright © 2021

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"errors"
	"fmt"
	"log"
	"os"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"strings"
)

var cfgFile string
var (
	splunkURL, splunkUser, splunkPassword, splunkDirectory string
)

var rootCmd = &cobra.Command{
	Use:   "splunkcli",
	Short: "This interfaces with Splunk REST API.",
	Long: `This is the fabulous Splunkcli tool.
Generally, you use it to do CRUD operations on 
a Splunk instance.`,
	//Run: func(cmd *cobra.Command, args []string) {
	//	//to get here, the command is executed without switches....
	//},
}

func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

//infile is STDIN, and used at this point to mock. Pass nil in on this for regular usage.
func Ask4confirm(message string, stdinfile *os.File) bool {
	if stdinfile == nil {
		stdinfile = os.Stdin
	}
	if !viper.GetBool("auto-approve") {
		var s string
		fmt.Printf("%s (y/N): ", message)

		//_, err := fmt.Scan(&s)
		_, err := fmt.Fscanf(stdinfile, "%s", &s)
		if err != nil {
			return false
		}
		s = strings.TrimSpace(s)
		s = strings.ToLower(s)

		if s == "y" || s == "yes" {
			return true
		}
		return false
	} else {
		return true
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.splunkcli.yaml)")
	rootCmd.PersistentFlags().Bool("auto-approve", false, "Approve without Y/N")

	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	viper.BindPFlag("auto-approve", rootCmd.PersistentFlags().Lookup("auto-approve"))

	rootCmd.PersistentFlags().String(
		"SPLUNK_URL",
		"",
		"URL For Splunk, with https:, and port")
	rootCmd.PersistentFlags().String(
		"SPLUNK_USER",
		"",
		"User to use for API calls to Splunk")
	rootCmd.PersistentFlags().String(
		"SPLUNK_PASSWORD",
		"",
		"Password to use for API calls to Splunk")
	rootCmd.PersistentFlags().String(
		"SPLUNK_DIRECTORY",
		"",
		"Directory to use for Splunk things")

	viper.BindPFlag("SPLUNK_URL", rootCmd.PersistentFlags().Lookup("SPLUNK_URL"))
	viper.BindPFlag("SPLUNK_USER", rootCmd.PersistentFlags().Lookup("SPLUNK_USER"))
	viper.BindPFlag("SPLUNK_PASSWORD", rootCmd.PersistentFlags().Lookup("SPLUNK_PASSWORD"))
	viper.BindPFlag("SPLUNK_DIRECTORY", rootCmd.PersistentFlags().Lookup("SPLUNK_DIRECTORY"))
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".splunkcli")
	}
	viper.AutomaticEnv() // read in environment variables that match
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
	collectFlags()
}
func collectFlags(){
	splunkURL = viper.GetString("SPLUNK_URL")
	splunkUser = viper.GetString("SPLUNK_USER")
	splunkPassword = viper.GetString("SPLUNK_PASSWORD")
	splunkDirectory = viper.GetString("SPLUNK_DIRECTORY")
	if splunkURL == "" || splunkUser == "" || splunkPassword == "" {
		log.Panic("You must set SPLUNK_URL, SPLUNK_USER, SPLUNK_PASSWORD somewhere, either flags, envvars, or in the config file")
	}
}
func exists(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}
func writeToDirectory(data string, objType string, filename string) bool{
	if splunkDirectory == "" || data == "" || filename == "" || objType == "" {
		return false
	}
	os.Mkdir(splunkDirectory, os.ModePerm)
	os.Mkdir(fmt.Sprintf("%s/%s", splunkDirectory, objType), os.ModePerm)
	fullFile := fmt.Sprintf("%s/%s/%s", splunkDirectory, objType, filename)
	err1 := os.Remove(fullFile)
	if err1 != nil {
		fmt.Printf(err1.Error())
	}
	if err := os.WriteFile(fullFile, []byte(data), 0666); err != nil {
		log.Fatal(err)
	}
	return true
}